CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
-------------

Provide field display formatter for file and image type fields.
Displays a link to download field instance contents as a .zip file.


REQUIREMENTS
-------------
This module requires the following modules:

Entity module (https://www.drupal.org/project/entity)
Transliteration module (https://www.drupal.org/project/transliteration)

This module requires Zip extension for PHP (https://secure.php.net/manual/en/book.zip.php)


INSTALLATION
--------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
--------------
* Go to "Manage display" section of a node or any other entity.
* Find you image or file field and change format to "Bulk download"
* You will see extra configuration options for your link by clicking the button
  in the far right.
* You can configure the link text that is shown as the "Download" link.
